package com.example.radek.pmex4;

public class StudentProjectGroup {
    private int studentId;
    private int groupId;

    public StudentProjectGroup(){

    }

    public StudentProjectGroup(int studentId, int groupId) {
        this.studentId = studentId;
        this.groupId = groupId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
