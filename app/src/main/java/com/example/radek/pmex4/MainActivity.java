package com.example.radek.pmex4;

import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private StudentAdapter myAdapter;
    private List<Student> studentsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CoordinatorLayout coordinatorLayout;
    private TextView noStudentsView;

    private StudentDatabase studentDatabase;
    private DataManagerImpl dataManagerImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        noStudentsView = findViewById(R.id.empty_students_view);

        studentDatabase = new StudentDatabase(this);
        dataManagerImpl = new DataManagerImpl(this);

        studentsList.addAll(dataManagerImpl.getStudentHeaders());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddStudentDialog(Boolean.parseBoolean(null), null, -1);
            }
        });

        myAdapter = new StudentAdapter(this, studentsList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));

        myAdapter.setClickListener(new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Student student = studentsList.get(position);
                showDetailsStudentDialog(student);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
        recyclerView.setAdapter(myAdapter);

        toogleEmptyStudents();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {
                        Student student = studentsList.get(position);
                        showDeleteStudentDialog(student, position);
                    }
                }));
    }

    private void showAddStudentDialog(boolean shouldUpdate, final Student student, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View view = layoutInflater.inflate(R.layout.add_student_dialog, null);

        final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        TextView dialogTitle = view.findViewById(R.id.add_dialog_title);
        final EditText inputName = view.findViewById(R.id.add_name);
        final EditText inputSurname = view.findViewById(R.id.add_surname);

        if(!shouldUpdate)
        dialogTitle.setText(getString(R.string.add_dialog_title));

        //tutaj ewentualnie zrobic opcje pomiedzy dodawaniem a modyfikacja (update) studenta
        alertDialogBuilderUserInput.setCancelable(false)
                .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(TextUtils.isEmpty(inputName.getText().toString()) || TextUtils.isEmpty(inputSurname.getText().toString())){
                            Toast.makeText(MainActivity.this, "Wprowadź studenta", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        else
                            alertDialogBuilderUserInput.create().dismiss();

                        if(student == null){
                            createStudent(inputName.getText().toString(), inputSurname.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        alertDialogBuilderUserInput.create();
        alertDialogBuilderUserInput.show();
    }

    public void showDeleteStudentDialog(final Student student, final int position){
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View view = layoutInflater.inflate(R.layout.delete_student_dialog, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);

        TextView deleteMessage = view.findViewById(R.id.delete_message);

        deleteMessage.setText(getString(R.string.delete_message) + " " + student.getName() + " " + student.getSurname() + "?");

        builder.setCancelable(false)
                .setPositiveButton("Usuń", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteChoiceStudent(student, position);
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.create();
        builder.show();
    }

    public void showDetailsStudentDialog(Student student){
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View view = layoutInflater.inflate(R.layout.details_student_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);

        TextView studentChoose = view.findViewById(R.id.details_dialog_student);
        TextView showStudentProjectGroups = view.findViewById(R.id.details_dialog_student_groups);

        studentChoose.setText(getString(R.string.details_dialog_student) + " " + student.getName() + " " + student.getSurname());

        List<ProjectGroup> projectGroupArrayList = dataManagerImpl.getStudentProjectGroups(student.getId());

        int i = 0;
        for (ProjectGroup projectGroup: projectGroupArrayList) {
            showStudentProjectGroups.append(" " + projectGroup.getGroupName());
            i++;
            if(i < projectGroupArrayList.size()){
                showStudentProjectGroups.append(",");
            }
        }


        builder.setCancelable(true);

        builder.create();
        builder.show();
    }

    private void createStudent(String name, String surname){
        Student student = new Student();
        student.setName(name);
        student.setSurname(surname);

        long id = dataManagerImpl.saveStudent(student);

        Toast.makeText(MainActivity.this, "Dodano studenta " + name + " " + surname, Toast.LENGTH_LONG).show();

        Student newStudent =  dataManagerImpl.getStudent(id);

        if (newStudent != null){
            studentsList.add(newStudent);
            myAdapter.notifyDataSetChanged();
            toogleEmptyStudents();
        }
    }

    private void deleteChoiceStudent(Student student, int position){
        Toast.makeText(MainActivity.this,"Usunięto "  + student.getName() + " " + student.getSurname(), Toast.LENGTH_LONG).show();

        dataManagerImpl.deleteStudent(student.getId());
        //TUTAJ WRZUCIC JESZCZE OBSLUGE USUWANIA Z BAZY DANYCH Z TABELI STUDENTPROJECTGROUP TAK BY NIE BYLO ID POWIAZANYCH Z TYM STUDENTEM

        studentsList.remove(position);
        myAdapter.notifyItemRemoved(position);

        toogleEmptyStudents();
    }

    public void toogleEmptyStudents(){
        if(studentDatabase.getStudentsCount() > 0){
            noStudentsView.setVisibility(View.GONE);
        }
        else
            noStudentsView.setVisibility(View.VISIBLE);
    }

    public void showActionDialog(final int position){

    }
}
