package com.example.radek.pmex4;

public class ProjectGroup {
    private int id;
    private String groupName;

    public ProjectGroup(){

    }

    public ProjectGroup(int id, String groupName) {
        this.id = id;
        this.groupName = groupName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
