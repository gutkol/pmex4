package com.example.radek.pmex4;

import java.util.List;

public interface DAO<T> {
    long save(T type);
    void update(T type);
    void delete(T type);
    T get(long id);
    List<T> getAll();
}
