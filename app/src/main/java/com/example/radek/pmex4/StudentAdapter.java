package com.example.radek.pmex4;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {
    private Context context;
    private List<Student> studentsList;
    private RecyclerTouchListener.ClickListener clickListener;
    private ProjectGroupAdapter projectGroupAdapter;
    private DataManagerImpl dataManager;
    private ProjectGroup projectGroup;

    public void setClickListener(RecyclerTouchListener.ClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView studentRow;
        public TextView dot;
        public Button editStudent;

        public MyViewHolder(View view, final RecyclerTouchListener.ClickListener clickListener){
            super(view);
            studentRow = view.findViewById(R.id.student_row);
            dot = view.findViewById(R.id.dot);
            editStudent = view.findViewById(R.id.edit_student_button);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();

                    if(position!=RecyclerView.NO_POSITION)
                    clickListener.onClick(view, position);
                }
            });
        }
    }

    public StudentAdapter(Context context, List<Student> studentsList){
        this.context = context;
        this.studentsList = studentsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.student_list_row, viewGroup, false);
        return new MyViewHolder(itemView, clickListener);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
        final int studentListPosition = i;
        final Student student = studentsList.get(i);
        dataManager = new DataManagerImpl(this.context);

        myViewHolder.dot.setText(Html.fromHtml("&#8226;"));

        myViewHolder.studentRow.setText(student.getName() + " " + student.getSurname());

        myViewHolder.editStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
                view = layoutInflater.inflate(R.layout.edit_student_dialog, null);

                final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setView(view);

                final EditText editName = view.findViewById(R.id.edit_name);
                final EditText editSurname = view.findViewById(R.id.edit_surname);

                final Spinner spinnerProjectsGroup = view.findViewById(R.id.spinner_project_group);
                ArrayList<ProjectGroup> projectGroupArrayList = new ArrayList<ProjectGroup>();
                projectGroupArrayList.addAll(dataManager.getAllProjectGroups());
                projectGroupAdapter = new ProjectGroupAdapter((Activity) view.getContext(),projectGroupArrayList);

                spinnerProjectsGroup.setAdapter(projectGroupAdapter);
                spinnerProjectsGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        projectGroup = (ProjectGroup) spinnerProjectsGroup.getItemAtPosition(i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                editName.setText(student.getName());
                editSurname.setText(student.getSurname());

                builder.setCancelable(true).setPositiveButton("Modyfikuj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        student.setName(editName.getText().toString());
                        student.setSurname(editSurname.getText().toString());
                        modifyStudent(student, projectGroup, studentListPosition);
                    }
                }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                builder.create();
                builder.show();
            }
        });
    }

    public void modifyStudent(Student student, ProjectGroup projectGroup, int studentListPosition){
        dataManager.updateStudent(student);

        StudentProjectGroup studentProjectGroup = new StudentProjectGroup(student.getId(), projectGroup.getId());
        if(!dataManager.existsStudentProjectGroup(studentProjectGroup)){
            dataManager.saveStudentProjectGroup(studentProjectGroup);
        }
        else
            dataManager.deleteStudentProjectGroup(studentProjectGroup);

        Toast.makeText(this.context, "Zaktualizowano studenta " + student.getName() + " " + student.getSurname(), Toast.LENGTH_LONG).show();

        studentsList.set(studentListPosition, student);
        notifyItemChanged(studentListPosition);
    }

    @Override
    public int getItemCount() {
        return studentsList.size();
    }
}
