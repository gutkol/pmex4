package com.example.radek.pmex4;

import java.util.List;

public interface DataManager {
    Student getStudent(long studentId);
    List<Student> getStudentHeaders();
    long saveStudent(Student student);
    boolean deleteStudent(long studentId);


    ProjectGroup getProjectGroup(long projectGroupId);
    List<ProjectGroup> getAllProjectGroups();
    long saveProjectGroup(ProjectGroup projectGroup);
    void deleteProjectGroup(ProjectGroup projectGroup);
    List<ProjectGroup> getStudentProjectGroups(long studentId);

    long saveStudentProjectGroup(StudentProjectGroup studentProjectGroup);
    void updateStudent(Student student);
    boolean existsStudentProjectGroup(StudentProjectGroup studentProjectGroup);
    void deleteStudentProjectGroup(StudentProjectGroup studentProjectGroup);
}
