package com.example.radek.pmex4;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;

import java.util.List;

public class DataManagerImpl implements DataManager {
    public static final int DATABASE_VERSION = 1;
    private String TAG = "MainActivity";

    private Context context;

    private SQLiteDatabase db;
    private StudentDAO studentDAO;
    private ProjectGroupDAO projectGroupDAO;
    private StudentProjectGroupDAO studentProjectGroupDAO;

    public DataManagerImpl(Context context){
        this.context = context;

        SQLiteOpenHelper openHelper = new StudentDatabase(this.context);
        db = openHelper.getWritableDatabase();

        studentDAO = new StudentDAO(db);
        projectGroupDAO = new ProjectGroupDAO(db);
        studentProjectGroupDAO = new StudentProjectGroupDAO(db);
    }

    @Override
    public Student getStudent(long studentId) {
        Student student = studentDAO.get(studentId);

        /*if(student != null){
            student.getProjectGroups().addAll(studentProjectGroupDAO.getProjectGroups(student.getId()));
        }*/

        return student;
    }

    @Override
    public List<Student> getStudentHeaders() {
        return studentDAO.getAll();
    }

    @Override
    public long saveStudent(Student student) {
        long studentId = 0L;

        /*try{
           db.beginTransaction();
           studentId = studentDAO.save(student);

           if(student.getProjectGroups().size() > 0){
               for(ProjectGroup c : student.getProjectGroups()){
                   long projectId = 0L;
                   ProjectGroup dbProject = projectGroupDAO.find(c.getGroupName());
                   if(dbProject == null)
                       projectId = projectGroupDAO.save(c);
                   else
                       projectId = dbProject.getId();
               }
           }
            db.setTransactionSuccessful();
        }
        catch (SQLException e){
            Log.e(TAG, "Błąd przy zapisie studenta (transakcje anulowano)", e);
            studentId = 0L;
        }
        finally {
            db.endTransaction();
        }*/

        // To poniezej dziala dla samego zapisu studenta
        db.beginTransaction();
        studentId = studentDAO.save(student);
        db.setTransactionSuccessful();
        db.endTransaction();

        return studentId;
    }

    @Override
    public boolean deleteStudent(long studentId) {
        boolean result = false;

        try{
            db.beginTransaction();
            Student student = getStudent(studentId);

            StudentProjectGroup studentProjectGroup = new StudentProjectGroup();
            studentProjectGroup.setStudentId(student.getId());

            if(student != null){
                studentDAO.delete(student);
                studentProjectGroupDAO.delete(studentProjectGroup);
            }
            db.setTransactionSuccessful();
            result = true;
        }
        catch(SQLException e){
            Log.e(TAG, "Błąd przy usuwaniu studenta (przerwano transakcję)", e);
        }
        finally {
            db.endTransaction();
        }

        return result;
    }

    @Override
    public ProjectGroup getProjectGroup(long projectGroupId) {
        return projectGroupDAO.get(projectGroupId);
    }

    @Override
    public List<ProjectGroup> getAllProjectGroups() {
        return projectGroupDAO.getAll();
    }

    @Override
    public long saveProjectGroup(ProjectGroup projectGroup) {
        return projectGroupDAO.save(projectGroup);
    }

    @Override
    public void deleteProjectGroup(ProjectGroup projectGroup) {
        projectGroupDAO.delete(projectGroup);
    }

    @Override
    public List<ProjectGroup> getStudentProjectGroups(long studentId){
        return studentProjectGroupDAO.getProjectGroups(studentId);
    }

    @Override
    public long saveStudentProjectGroup(StudentProjectGroup studentProjectGroup) {
         return studentProjectGroupDAO.save(studentProjectGroup);
    }

    @Override
    public void updateStudent(Student student) {
        studentDAO.update(student);
    }

    @Override
    public boolean existsStudentProjectGroup(StudentProjectGroup studentProjectGroup) {
        if (studentProjectGroup.getStudentId() > 0 && studentProjectGroup.getGroupId() > 0)
        {
            Cursor cursor = db.query(StudentDatabase.StudentProjectGroupTable.TABLE_NAME, new String[]{
                    StudentDatabase.StudentProjectGroupTable.STUDENT_ID, StudentDatabase.StudentProjectGroupTable.PROJECT_GROUP_ID },
                    StudentDatabase.StudentProjectGroupTable.STUDENT_ID + " = ? AND " + StudentDatabase.StudentProjectGroupTable.PROJECT_GROUP_ID + " = ?",
                    new String[]{String.valueOf(studentProjectGroup.getStudentId()), String.valueOf(studentProjectGroup.getGroupId())},
                    null, null, null, null);

            if (cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            else
                return true;
        }
        return false;
    }

    @Override
    public void deleteStudentProjectGroup(StudentProjectGroup studentProjectGroup) {
        studentProjectGroupDAO.deleteProjectGroupFromStudent(studentProjectGroup);
    }

}
