package com.example.radek.pmex4;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

public class StudentProjectGroupDAO implements DAO<StudentProjectGroup>{
    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public static final String INSERT = "INSERT INTO " + StudentDatabase.StudentProjectGroupTable.TABLE_NAME
            + "("+ StudentDatabase.StudentProjectGroupTable.STUDENT_ID + ", " + StudentDatabase.StudentProjectGroupTable.PROJECT_GROUP_ID +")"
            + " values (?, ?)";

    public StudentProjectGroupDAO(SQLiteDatabase db) {
        this.db = db;
        this.insertStatement = db.compileStatement(INSERT);
    }

    @Override
    public long save(StudentProjectGroup type) {
        insertStatement.clearBindings();
        insertStatement.bindLong(1, type.getStudentId());
        insertStatement.bindLong(2, type.getGroupId());

        return insertStatement.executeInsert();
    }

    @Override
    public void update(StudentProjectGroup type) {

    }

    @Override
    public void delete(StudentProjectGroup type) {
        if (type.getStudentId() >0)
        db.delete(StudentDatabase.StudentProjectGroupTable.TABLE_NAME,
                StudentDatabase.StudentProjectGroupTable.STUDENT_ID + " = ?",
                new String[]{String.valueOf(type.getStudentId())});
    }

    public void deleteProjectGroupFromStudent(StudentProjectGroup type){
        if(type.getStudentId() > 0 && type.getGroupId() > 0)
            db.delete(StudentDatabase.StudentProjectGroupTable.TABLE_NAME,
                    StudentDatabase.StudentProjectGroupTable.STUDENT_ID + " = ? AND "
                            + StudentDatabase.StudentProjectGroupTable.PROJECT_GROUP_ID + " = ?",
                    new String[]{String.valueOf(type.getStudentId()), String.valueOf(type.getGroupId())});
    }

    @Override
    public StudentProjectGroup get(long id) {
        return null;
    }

    @Override
    public List<StudentProjectGroup> getAll() {
        return null;
    }

    public List<ProjectGroup> getProjectGroups(long id) {
        List<ProjectGroup> projectGroupsList = new ArrayList<>();

        String selectQuery = "SELECT " + StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME + " FROM " + StudentDatabase.StudentTable.TABLE_NAME + " st, "
                + StudentDatabase.ProjectGroupTable.TABLE_NAME + " pgt, "
                + StudentDatabase.StudentProjectGroupTable.TABLE_NAME + " spgt WHERE st."
                + StudentDatabase.StudentTable.COLUMN_STUDENT_ID + " = spgt." + StudentDatabase.StudentProjectGroupTable.STUDENT_ID + " AND "
                + "pgt." + StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID + " = spgt." + StudentDatabase.StudentProjectGroupTable.PROJECT_GROUP_ID
                + " AND spgt." + StudentDatabase.StudentProjectGroupTable.STUDENT_ID + " = " + id;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do{
                ProjectGroup projectGroup = new ProjectGroup();
                projectGroup.setGroupName(cursor.getString(cursor.getColumnIndex(StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME)));

                projectGroupsList.add(projectGroup);
            }while(cursor.moveToNext());
        }

        if (!cursor.isClosed())
            cursor.close();

        return projectGroupsList;
    }
}
