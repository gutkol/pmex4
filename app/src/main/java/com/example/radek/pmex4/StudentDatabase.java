package com.example.radek.pmex4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;


public class StudentDatabase extends SQLiteOpenHelper {

    //utworzenie tabeli student z kolumnami
    public class StudentTable {
        public static final String TABLE_NAME = "Students";

        public static final String COLUMN_STUDENT_ID = "Id";
        public static final String NAME = "Imie";
        public static final String SURNAME = "Nazwisko";

//        Set<ProjectGroup> projectGroups; //to ma byc dodane do osobnego klasy modelu Student

        public static final String SQL_CREATE_TABLE_STUDENT = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_STUDENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME + " TEXT NOT NULL, "
                + SURNAME + " TEXT NOT NULL"
                + ");";
    }

    public class ProjectGroupTable {
        public static final String TABLE_NAME = "ProjectGroups";

        public static final String COLUMN_PROJECT_ID = "Id";
        public static final String PROJECT_GROUP_NAME = "Nazwa";

        public static final String SQL_CREATE_TABLE_PROJECT_GROUP = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_PROJECT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PROJECT_GROUP_NAME + " TEXT UNIQUE NOT NULL"
                + ");";
    }

    public class StudentProjectGroupTable {
        public static final String TABLE_NAME = "StudentProjectGroups";

        public static final String STUDENT_ID = "Id_studenta";
        public static final String PROJECT_GROUP_ID = "Id_grupy";

        public static final String SQL_CREATE_TABLE_STUDENT_PROJECT_GROUP_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + STUDENT_ID + " INTEGER NOT NULL, "  + PROJECT_GROUP_ID + " INTEGER NOT NULL,"
                + " FOREIGN KEY(" + STUDENT_ID + ") REFERENCES " + StudentTable.TABLE_NAME + "(" + StudentTable.COLUMN_STUDENT_ID + "),"
                + " FOREIGN KEY(" + PROJECT_GROUP_ID + ") REFERENCES " + ProjectGroupTable.TABLE_NAME + "(" + ProjectGroupTable.COLUMN_PROJECT_ID + "),"
                + " PRIMARY KEY(" + STUDENT_ID + ", " + PROJECT_GROUP_ID + ")"
                + ");";
    }



    private static final String DATABASE_NAME = "StudentDatabase.db";

    public StudentDatabase(Context context) {
        super(context, DATABASE_NAME, null, DataManagerImpl.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(StudentTable.SQL_CREATE_TABLE_STUDENT);
        sqLiteDatabase.execSQL(ProjectGroupTable.SQL_CREATE_TABLE_PROJECT_GROUP);
        sqLiteDatabase.execSQL(StudentProjectGroupTable.SQL_CREATE_TABLE_STUDENT_PROJECT_GROUP_TABLE);

        String[] groups = new String[]{"Projektowa", "Wykonawcza", "Techniczna", "Architektoniczna", "Informatyczna", "Zadaniowa"};
        ContentValues values = new ContentValues();
        for (int i = 0; i < groups.length; i++){
            values.put(ProjectGroupTable.PROJECT_GROUP_NAME, groups[i]);
            sqLiteDatabase.insert(ProjectGroupTable.TABLE_NAME, null, values);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + StudentTable.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ProjectGroupTable.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + StudentProjectGroupTable.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

    public int getStudentsCount(){
        String countQuery = "SELECT * FROM " + StudentTable.TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }
}
