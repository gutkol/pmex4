package com.example.radek.pmex4;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ProjectGroupAdapter extends BaseAdapter {

    public List<ProjectGroup> projectGroupList;
    public Activity activity;
    public LayoutInflater layoutInflater;

    public ProjectGroupAdapter(Activity activity, List<ProjectGroup> paramList){
        this.activity = activity;
        this.projectGroupList = paramList;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return projectGroupList.size();
    }

    @Override
    public Object getItem(int i) {
        return projectGroupList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = layoutInflater.inflate(R.layout.spinner_dropdown_row_project_group, null);

        TextView projectTitle = row.findViewById(R.id.spinner_row);
        ProjectGroup spinProjectGroup = projectGroupList.get(i);
        projectTitle.setText(spinProjectGroup.getGroupName());

        return row;
    }
}
