package com.example.radek.pmex4;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

public class StudentDAO implements DAO<Student> {

    public static final String INSERT = "INSERT INTO " + StudentDatabase.StudentTable.TABLE_NAME
            + "(" + StudentDatabase.StudentTable.NAME + ", "
            + StudentDatabase.StudentTable.SURNAME + ")"
            + " values(?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public StudentDAO(SQLiteDatabase db){
        this.db = db;
        insertStatement = db.compileStatement(INSERT);
    }

    @Override
    public long save(Student type) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, type.getName());
        insertStatement.bindString(2, type.getSurname());

        return insertStatement.executeInsert();
    }

    @Override
    public void update(Student type) {
        final ContentValues values = new ContentValues();

        values.put(StudentDatabase.StudentTable.NAME, type.getName());
        values.put(StudentDatabase.StudentTable.SURNAME, type.getSurname());
        db.update(StudentDatabase.StudentTable.TABLE_NAME, values, StudentDatabase.StudentTable.COLUMN_STUDENT_ID + " = ? ",
                new String[]{String.valueOf(type.getId())});
    }

    @Override
    public void delete(Student type) {
        if(type.getId() > 0){
            db.delete(StudentDatabase.StudentTable.TABLE_NAME, StudentDatabase.StudentTable.COLUMN_STUDENT_ID + " = ?",
                    new String[]{String.valueOf(type.getId())});
        }
    }

    @Override
    public Student get(long id) {
        Cursor c = db.query(StudentDatabase.StudentTable.TABLE_NAME, new String[]{
                StudentDatabase.StudentTable.COLUMN_STUDENT_ID, StudentDatabase.StudentTable.NAME, StudentDatabase.StudentTable.SURNAME },
                StudentDatabase.StudentTable.COLUMN_STUDENT_ID + " = ?", new String[]{String.valueOf(id)},
                null,null,null, null );

        if (c != null){
            c.moveToFirst();
        }

        Student studentModel = new Student(
                c.getInt(c.getColumnIndex(StudentDatabase.StudentTable.COLUMN_STUDENT_ID)),
                c.getString(c.getColumnIndex(StudentDatabase.StudentTable.NAME)),
                c.getString(c.getColumnIndex(StudentDatabase.StudentTable.SURNAME))
        );

        c.close();

        return studentModel;
    }

    @Override
    public List<Student> getAll() {
        List<Student> studentModelList = new ArrayList<>();

        Cursor c = db.query(StudentDatabase.StudentTable.TABLE_NAME, new String[]{
                StudentDatabase.StudentTable.COLUMN_STUDENT_ID, StudentDatabase.StudentTable.NAME, StudentDatabase.StudentTable.SURNAME},
                null, null, null, null, null , null);

        if(c.moveToFirst()) {
            do {
                Student studentModel = new Student();
                studentModel.setId(c.getInt(c.getColumnIndex(StudentDatabase.StudentTable.COLUMN_STUDENT_ID)));
                studentModel.setName(c.getString(c.getColumnIndex(StudentDatabase.StudentTable.NAME)));
                studentModel.setSurname(c.getString(c.getColumnIndex(StudentDatabase.StudentTable.SURNAME)));

                studentModelList.add(studentModel);
            } while(c.moveToNext());
        }

        if(!c.isClosed())
            c.close();

        return studentModelList;
    }

   /* public Student find(String name){
        long studentId = 0L;
        String sql = "SELECT Id FROM " + StudentDatabase.StudentTable.TABLE_NAME
                + " WHERE UPPER(" + StudentDatabase.StudentTable.NAME + ") = ? limit 1";
        Cursor c = db.rawQuery(sql, new String[]{name.toUpperCase()});
        if(c.moveToFirst()){
            studentId = c.getLong(0);
        }
        if(!c.isClosed())
            c.close();

        return  get(studentId);
    }*/
}
