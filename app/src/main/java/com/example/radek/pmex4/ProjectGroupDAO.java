package com.example.radek.pmex4;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

public class ProjectGroupDAO implements DAO<ProjectGroup>{
    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public static final String INSERT = "INSERT INTO " + StudentDatabase.ProjectGroupTable.TABLE_NAME
            + "(" + StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME + ")"
            + " values (?)";

    ProjectGroupDAO (SQLiteDatabase db){
        this.db = db;
        insertStatement = db.compileStatement(INSERT);
    }

    @Override
    public long save(ProjectGroup type) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, type.getGroupName());

        return insertStatement.executeInsert();
    }

    @Override
    public void update(ProjectGroup type) {

    }

    @Override
    public void delete(ProjectGroup type) {

    }

    @Override
    public ProjectGroup get(long id) {
        Cursor c = db.query(StudentDatabase.ProjectGroupTable.TABLE_NAME, new String[]{
                StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID, StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME},
                StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID + " = ? ", new String[]{String.valueOf(id)},
                null, null,null,null);

        if (c != null){
            c.moveToFirst();
        }

        ProjectGroup projectGroupModel = new ProjectGroup(
                c.getInt(c.getColumnIndex(StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID)),
                c.getString(c.getColumnIndex(StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME))
        );

        c.close();

        return projectGroupModel;
    }

    @Override
    public List<ProjectGroup> getAll() {
        List<ProjectGroup> projectModelList = new ArrayList<>();

        Cursor c = db.query(StudentDatabase.ProjectGroupTable.TABLE_NAME, new String[]{
                        StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID, StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME},
                null, null, null, null, null , null);

        if(c.moveToFirst()) {
            do {
                ProjectGroup projectModel = new ProjectGroup();
                projectModel.setId(c.getInt(c.getColumnIndex(StudentDatabase.ProjectGroupTable.COLUMN_PROJECT_ID)));
                projectModel.setGroupName(c.getString(c.getColumnIndex(StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME)));

                projectModelList.add(projectModel);
            } while(c.moveToNext());
        }

        if(!c.isClosed())
            c.close();
        return projectModelList;
    }

   /* public ProjectGroup find(String groupName) {
        long projectId = 0L;
        String sql = "SELECT _id FROM " + StudentDatabase.ProjectGroupTable.TABLE_NAME
                + " WHERE UPPER(" + StudentDatabase.ProjectGroupTable.PROJECT_GROUP_NAME + ") = ? limit 1";
        Cursor c = db.rawQuery(sql, new String[]{groupName.toUpperCase()});
        if(c.moveToFirst()){
            projectId = c.getLong(0);
        }
        if(!c.isClosed())
            c.close();

        return  get(projectId);
    }*/
}
